#!/bin/bash

# On verifie si le script est lance avec les droits root
if [ "$EUID" -eq 0 ]

        then

        read -p "Quel service voulez-vous verifier ?" target_service

        # On utilise la fonction is-active de systemctl pour verifier l etat
        systemctl is-active $target_service

else

        # On demande a l utilisateur de relancer le script s il n a pas les droits root
        echo "Relancer le script en tant que sudo."

fi
