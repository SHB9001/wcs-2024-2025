#!/bin/bash

read -p "Quelle est votre ip a tester ? " tested_ip

# On va decouper l IP 5 fois pour s assurer qu il n y a pas trop de blocks
for num in 1 2 3 4 5
    do
        # On decoupe le block en fonction de l indexe de la boucle
        tested_block=$(echo $tested_ip | cut -f $num -d '.')

        # On verifie que le premier block n est pas egale a 0
        if [ "$num" -eq 1 ] && [ "$tested_block" -eq 0 ]; then
            echo "Addresse invalide, le premier block ne peut etre egale a 0."
            exit 0

        # On verifie qu il n y a pas de block vide
        elif [ "$num" -ne 5 ] && [ -z $tested_block ]; then
            echo "Addresse invalide, les blocks ne peuvent etre vides."
            exit 0

        # On verifie que le block est ni vide, ni superieur a 255, ni inferieur a 0
        elif [ "$num" -ne 5 ] && { [ "$tested_block" -lt 0 ] || [ "$tested_block" -gt 255 ]; }; then
            echo "Addresse invalide, les blocks doivent entre 0-255."
            exit 0
        
        # On verifie qu il n y a pas de 5e block
        elif [ "$num" -eq 5 ] && [ -n "$tested_block" ]; then
            echo "Addresse invalide, il y a trop de blocks."
            exit 0
        fi
    done

# Si aucun test n a interrompu le script on en deduit que l ip est bonne
echo "Adresse valide."
