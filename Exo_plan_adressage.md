# Plan en mode Asymetrique

Pour un réseau 172.16.1.0/24 .
On tiendra compte des besoins propres a chaque Poloe pour définir la taille du sous reseau.

|Pole|Nb machines |Taille binaire|Reseau|Broadcast|Adresse debut|Adresse fin|
|---|---|---|---|---|---|---|
|Informatique|50|64|172.16.1.0|.63|.1|.62|
|Developpement|12|16|172.16.1.64|.79|.65|.78|
|Administratif|20|32|172.16.1.80|.111|.81|.110|
|Technicien|15|32|172.16.1.112|.133|.113|.132|

# Plan en mode Asymetrique

Pour un réseau 172.16.1.0/24 .
Le plus grand sous reseau etant celui du Pole Informatique, son besoin va definir la taille des autres sous reseaux.

|Pole|Nb machines |Taille binaire|Reseau|Broadcast|Adresse debut|Adresse fin|
|---|---|---|---|---|---|---|
|Informatique|50|64|172.16.1.0|.63|.1|.62|
|Developpement|12|64|172.16.1.64|.127|.65|.126|
|Administratif|20|64|172.16.1.128|.191|.129|.190|
|Technicien|15|64|172.16.1.192|.255|.193|.254|